﻿using System;
using System.Diagnostics;
using System.IO;

namespace Temail2016_Updater
{
    class Program
    {
        static void Main(string[] args)
        {

            checkProcesses();
            delAndCopyFiles();
            startTEmail();

        }
        
        static void checkProcesses()
        {
            try
            {
                //make sure TEmail is closed
                Process[] runningProcesses = Process.GetProcesses();
                foreach (Process process in runningProcesses)
                {
                    // now check the modules of the process
                    foreach (ProcessModule module in process.Modules)
                    {
                        if (module.FileName.Equals("TEmail2016.exe"))
                        {
                            process.Kill();
                        }
                    }
                }
            }catch(System.Exception e){
                Console.WriteLine("Error handling TEmail2016.exe: ");
                Console.WriteLine(e.Message);
            }
        }

        static void delAndCopyFiles()
        {
            DirectoryInfo dirInfo;
            try
            {
                dirInfo = new DirectoryInfo(@"C:\Temail2016");
                dirInfo.Delete(true);

            }catch(System.Exception e)
            {
                Console.WriteLine("Cannot delete directories: ");
                Console.WriteLine(e.Message);
            }

            DirectoryCopy(@"\\hopfileshare02\hodasd\Temporary\carl\TEmail2016", @"C:\Temail2016", true);
        }


        static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            try
            {
                // Get the subdirectories for the specified directory.
                DirectoryInfo dir = new DirectoryInfo(sourceDirName);

                if (!dir.Exists)
                {
                    throw new DirectoryNotFoundException(
                        "Source directory does not exist or could not be found: "
                        + sourceDirName);
                }

                DirectoryInfo[] dirs = dir.GetDirectories();

                // If the destination directory doesn't exist, create it.       
                Directory.CreateDirectory(destDirName);

                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string tempPath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(tempPath, false);
                }

                // If copying subdirectories, copy them and their contents to new location.
                if (copySubDirs)
                {
                    foreach (DirectoryInfo subdir in dirs)
                    {
                        string tempPath = Path.Combine(destDirName, subdir.Name);
                        DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                    }
                }
            }catch(System.Exception e)
            {
                Console.WriteLine("Cannot copy directory:");
                Console.WriteLine(e.Message);
            }
        }

        static void startTEmail()
        {
            try
            {
                Process.Start(@"C:\Temail2016\TEmail2016.exe");
            }catch(System.Exception e)
            {
                Console.WriteLine("Cannot open Temail:");
                Console.WriteLine(e.Message);
            }
        }
    }



}
