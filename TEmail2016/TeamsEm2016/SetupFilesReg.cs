﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.IO;

namespace TEmail2016
{
    class SetupFilesReg
    {
         NetworkMessage networkMessage;
        public SetupFilesReg(NetworkMessage nwMessage)
        {
            networkMessage = nwMessage;
        }

         public void createShortcut()
        {
            var appdataRoamingPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            try
            {
                if (!File.Exists(appdataRoamingPath + @"\Microsoft\Windows\Start Menu\Programs\Startup\TEmail2016.lnk"))
                {
                    Console.WriteLine("Copying startup link...");
                    File.Copy("TEmail2016.lnk", appdataRoamingPath + @"\Microsoft\Windows\Start Menu\Programs\Startup\TEmail2016.lnk");
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Unable to copy startup shortcut.");
                Console.WriteLine(e.Message);
                networkMessage.addException(new ExceptionNetwork(e.Message));
            }
            finally
            {

            }
        }

        public void setReg()
        {
            try
            {
                RegistryKey key;
                key = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Office\16.0\Outlook\Preferences", true);
                key.SetValue("NewMailDesktopAlerts", 0, RegistryValueKind.DWord);
                key.SetValue("playsound", 0, RegistryValueKind.DWord);
                key.SetValue("showenvelope", 0, RegistryValueKind.DWord);
                key.SetValue("changepointer", 0, RegistryValueKind.DWord);
                key.SetValue("ApptReminders", 0, RegistryValueKind.DWord);
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Can't write to outlook registry preferences.");
                Console.WriteLine(e.Message);
                networkMessage.addException(new ExceptionNetwork(e.Message));
            }
            finally
            {

            }
        }

    }
}
