﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace TEmail2016_Helper
{
    class NetworkMessage
    {
        int port;
        //string message = Environment.MachineName;
        List<Device> devices;

        public NetworkMessage()
        {
            setServer();
            devices = new List<Device>();
            readClients();
            

        }

        public List<Device> getDevices()
        {
            return devices;
        }

        public Device getDeviceByName(string devName)
        {
            foreach (var device in devices)
            {
                if (device.name.ToUpper() == devName.ToUpper())
                {
                    return device;
                }
            }
            return null;
        }

        private void readClients()
        {
            try
            {
                foreach(var line in File.ReadAllLines("devices.txt"))
                {
                    if (!line.Contains(@"//")) 
                        devices.Add(new Device(line));
                }
            }
            catch
            {
                Console.WriteLine("Could not read device.txt.");
            }
        }

        private void setServer()
        {
            var lines = File.ReadAllLines("socket.txt");
            port = int.Parse(lines[0]);
        }

        public async Task recieve()
        {
            TcpListener server = null;
            try
            {


                // TcpListener server = new TcpListener(port);
                server = new TcpListener(IPAddress.Any, port);

                // Start listening for client requests.
                server.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[256];
                String data = null;

                // Enter the listening loop.
                while (true)
                {
                    Console.WriteLine("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    // You could also use server.AcceptSocket() here.
                     TcpClient client = await server.AcceptTcpClientAsync();
                    Console.WriteLine("Connected!");

                    data = null;

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int i;

                    // Loop to receive all the data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received: {0}", data);
                        // Process the data sent by the client.
                        data = data.ToUpper();

                        byte[] msg = System.Text.Encoding.ASCII.GetBytes("Y");

                        // Send back a response.
                        stream.Write(msg, 0, msg.Length);
                        Console.WriteLine("Sent: {0}", data);
                    }

                    // Shutdown and end connection
                    client.Close();
                }
            }
            catch (SystemException e)
            {
                Console.WriteLine("SocketException: ", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
                recieve();
            }
        }

        async void checkAll(string message)
        {
        foreach (Device device in devices)
        {
            var response = await send(device.name, message);
            if (response == "")
            {
                Console.WriteLine(device.name + ": - - - Offline - - - ");
            }
            else
            {
                Console.WriteLine(device.name + ": " + response);
            }
            }
            Console.WriteLine("Scanned all devices.");
        }

        public async Task<string> send(string server, string message)
        {
            TcpClient client = null;
            NetworkStream stream = null;
            // String to store the response ASCII representation.
            String responseData = String.Empty;

            try
            {
                // Create a TcpClient.
                client = new TcpClient(server, port);
                
                // Translate the passed message into ASCII and store it as a Byte array.
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

                // Get a client stream for reading and writing.
                //  Stream stream = client.GetStream();
               
                stream = client.GetStream();

                stream.WriteTimeout = 1 * 2000;
                // Send the message to the connected TcpServer.
                stream.Write(data, 0, data.Length);

               // Console.WriteLine("Sent: {0}", message);

                // Buffer to store the response bytes.
                data = new Byte[256];


                // Read the first batch of the TcpServer response bytes.
                stream.ReadTimeout = 1 * 2000;
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                //Console.WriteLine("Received: {0}", responseData);

                // Close everything.
                stream.Close();
                client.Close();

                return (responseData);

            }
            catch (System.Exception e)
            {
               // Console.WriteLine(e);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                if (client != null)
                    client.Close();
            }
            return (responseData);
        }


    }
}
